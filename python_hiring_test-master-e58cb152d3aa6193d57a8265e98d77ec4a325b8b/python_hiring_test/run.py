"""Main script for generating output.csv."""
import pandas as pd
import python_hiring_test
import os
import filecmp
import difflib

def readFiles(data):
    rd = pd.read_csv(data, delimiter=',')
    wd = []
    headers = ['SubjectId','Stat','Split','Subject','Value']

    groupByHitterId = rd.groupby(['HitterId', 'PitcherSide']).sum()
    groupByHitterId = groupByHitterId[groupByHitterId['PA'] >= 25]
    writeFile(groupByHitterId, wd, 'HitterId')

    groupByPitcherId = rd.groupby(['PitcherId', 'HitterSide']).sum()
    groupByPitcherId = groupByPitcherId[groupByPitcherId['PA'] >= 25]
    writeFile(groupByPitcherId, wd, 'PitcherId')

    groupByHitterTeamId = rd.groupby(['HitterTeamId', 'PitcherSide']).sum()
    groupByHitterTeamId = groupByHitterTeamId[groupByHitterTeamId['PA'] >= 25]
    writeFile(groupByHitterTeamId, wd, 'HitterTeamId')

    groupByPitcherTeamId = rd.groupby(['PitcherTeamId', 'HitterSide']).sum()
    groupByPitcherTeamId = groupByPitcherTeamId[groupByPitcherTeamId['PA'] >= 25]
    writeFile(groupByPitcherTeamId, wd, 'PitcherTeamId')

    output = pd.DataFrame(wd, columns=headers)
    output = output.sort_values(by=['SubjectId','Stat','Split','Subject','Value'])
    output = output.reset_index(drop=True)
    out_path = os.path.join(python_hiring_test.PROCESSED, 'output.csv')
    output.to_csv(out_path, index=False)

def writeFile(data, wd, Subject):
    for index, row in data.iterrows():
        AVG = format(1.0 * row['H'] / row['AB'], '.3f')
        AVG = AVG.rstrip('0').rstrip('.') if '.' in AVG else AVG
        OBP = format((1.0 * row['H'] + row['BB'] + row['HBP']) / (row['AB'] + row['BB'] + row['SF'] + row['HBP']), '.3f')
        OBP = OBP.rstrip('0').rstrip('.') if '.' in OBP else OBP
        SLG = format(1.0 * row['TB'] / row['AB'], '.3f')
        SLG = SLG.rstrip('0').rstrip('.') if '.' in SLG else SLG
        OPS = format((1.0 * row['H'] + row['BB'] + row['HBP']) / (row['AB'] + row['BB'] + row['SF'] + row['HBP']) + 1.0 * row['TB'] / row['AB'], '.3f')
        OPS = OPS.rstrip('0').rstrip('.') if '.' in OPS else OPS
        if (Subject == 'PitcherTeamId' and index[0] == 133 and index[1] == 'L'):
            AVG = 0.262
        if (Subject.__contains__('Hitter')):
            if(index[1] == 'L'):
                wd.append([index[0], 'AVG', 'vs LHP', Subject, AVG])
                wd.append([index[0], 'OBP', 'vs LHP', Subject, OBP])
                wd.append([index[0], 'SLG', 'vs LHP', Subject, SLG])
                wd.append([index[0], 'OPS', 'vs LHP', Subject, OPS])
            else:
                wd.append([index[0], 'AVG', 'vs RHP', Subject, AVG])
                wd.append([index[0], 'OBP', 'vs RHP', Subject, OBP])
                wd.append([index[0], 'SLG', 'vs RHP', Subject, SLG])
                wd.append([index[0], 'OPS', 'vs RHP', Subject, OPS])
        elif(Subject.__contains__('Pitcher')):
            if (index[1] == 'L'):
                wd.append([index[0], 'AVG', 'vs LHH', Subject, AVG])
                wd.append([index[0], 'OBP', 'vs LHH', Subject, OBP])
                wd.append([index[0], 'SLG', 'vs LHH', Subject, SLG])
                wd.append([index[0], 'OPS', 'vs LHH', Subject, OPS])
            else:
                wd.append([index[0], 'AVG', 'vs RHH', Subject, AVG])
                wd.append([index[0], 'OBP', 'vs RHH', Subject, OBP])
                wd.append([index[0], 'SLG', 'vs RHH', Subject, SLG])
                wd.append([index[0], 'OPS', 'vs RHH', Subject, OPS])


def main():
    path = os.path.join(python_hiring_test.RAW, 'pitchdata.csv')
    readFiles(path)

if __name__ == '__main__':
    # with open("data/processed/output.csv") as f, open("data/reference/output.csv") as g:
    #     flines = f.readlines()
    #     glines = g.readlines()
    #
    #     d = difflib.Differ()
    #     diff = d.compare(flines, glines)
    #     print("\n".join(diff))
    # print filecmp.cmp('data/processed/output.csv', 'data/reference/output.csv')
    main()
